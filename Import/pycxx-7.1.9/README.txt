Version: 7.1.9 (9-Sep-2023)

Add support for building against python 3.12 RC2

The following functions have been remove from 3.12 builds:
    int &_Py_DebugFlag()
    int &_Py_InteractiveFlag()
    int &_Py_OptimizeFlag()
    int &_Py_NoSiteFlag()
    int &_Py_VerboseFlag()

They depend on deprecated variables:
    Py_DebugFlag, Py_InteractiveFlag, Py_OptimizeFlag
    Py_NoSiteFlag, Py_VerboseFlag

If the value of these variables is needed they can be accessed
from sys.flags.

_Py_PackageContext is no longer accessible.
