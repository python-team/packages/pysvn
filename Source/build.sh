#!/bin/bash
set -e

if [ "$1" != "" ]
then
    PY_VER=$1
    shift
fi

if [ "$1" != "" ]
then
    SVN_VER=$1
    shift
fi

case "$(uname)" in
Linux)
    python${PY_VER} setup.py configure \
        --enable-debug \
        --verbose \
        "$@"
    ;;

Darwin)
    export MACOSX_DEPLOYMENT_TARGET=11.0

    python${PY_VER} setup.py configure \
        --distro-dir=/usr/local/svn-${SVN_VER} \
        --distro-dir=/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.11.sdk/usr \
        --arch=x86_64 --arch=arm64 \
        --pycxx-dir=${BUILDER_TOP_DIR}/Import/pycxx-${PYCXX_VER} \
        --define=APR_IOVEC_DEFINED \
        --verbose \
        "$@"
    ;;
*)
    echo "Error: need support for $(uname)"
    exit 1
    ;;

esac

make clean
make all

cd ../Tests
make clean
make all
